package API.management;

import API.db.TMDatabase;
import com.mongodb.DB;

/**
 * @Project - TIO
 * User: Guilherme
 * Date: 4/21/2014 - 6:13 PM
 * API.management - <Description>
 */
public class EstadioManagement extends ManagementObject implements ManagementObjectInterface {

    public EstadioManagement(TMDatabase databaseClient) {
        DB db = databaseClient.getDB();
        collection = db.getCollection("estadio");
    }
}
