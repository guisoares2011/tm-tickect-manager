package API.management;

import API.db.TMDatabase;
import com.mongodb.*;

import java.util.ArrayList;

/**
 * @Project - MTP
 * User: Guilherme
 * Date: 4/10/2014 - 10:03 PM
 * API.management - <Description>
 */
public class ManagementObject {

    public DBCollection collection;
    public DB db;
    public TMDatabase databaseClient;

    protected ArrayList<DBObject> find(BasicDBObject data) {
        ArrayList<DBObject> response = new ArrayList<DBObject>();
        DBCursor cursor = collection.find(data);
        while (cursor.hasNext()) {
            response.add(cursor.next());
        }
        return response;
    }

    protected DBObject findOne(BasicDBObject data) {
        ArrayList<DBObject> response = this.find(data);
        if (response.size() == 0) {
            return null;
        } else {
            return response.get(0);
        }
    }

    protected Object createInstanceObjectBy(DBObject data) {
        return data.get("nome").toString();
    }

    public ArrayList<Object> getAll() {
        ArrayList<Object> list = new ArrayList<Object>();
        DBCursor cursor = collection.find();

        int i = 0;
        while (cursor.hasNext()) {
            list.add(i, createInstanceObjectBy(cursor.next()));
        }

        return list;
    }
}
