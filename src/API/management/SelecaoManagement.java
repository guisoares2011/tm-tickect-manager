package API.management;

import API.db.TMDatabase;
import com.mongodb.DB;

/**
 * @Project - TIO
 * User: Guilherme
 * Date: 4/21/2014 - 12:09 PM
 * API.management - <Description>
 */

public class SelecaoManagement extends ManagementObject implements ManagementObjectInterface {

    public SelecaoManagement(TMDatabase databaseClient) {
        DB db = databaseClient.getDB();
        collection = db.getCollection("selecao");
    }
}
