package API.management;

import API.db.TMDatabase;
import API.objects.Ingresso;
import API.objects.Partida;
import com.mongodb.*;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Project - TIO
 * User: Guilherme
 * Date: 4/21/2014 - 10:40 PM
 * API.management - <Description>
 */
public class PartidaManagement extends ManagementObject implements ManagementObjectInterface {

    public PartidaManagement(TMDatabase databaseClient) {
        this.databaseClient = databaseClient;
        db = databaseClient.getDB();
        collection = db.getCollection("partidas");
    }

    public ArrayList<Partida> get(Partida p) {
        ArrayList<DBObject> response = find(this.getObjectDataDB(p));
        ArrayList<Partida> partidas = new ArrayList<Partida>();
        for (int i = 0; i < response.size(); i++) {
            partidas.add(this.createInstanceObjectBy(response.get(i)));
        }
        return partidas;
    }

    public Partida getOne(Partida partida) {
        BasicDBObject info = this.getObjectDataDB(partida);
        DBObject response = this.findOne(info);
        return response == null ? null : this.createInstanceObjectBy(response);
    }

    @Override
    protected Partida createInstanceObjectBy(DBObject response) {
        Partida partida = new Partida();
        partida.setDbObject(response);
        partida.setTitulo(response.get("titulo").toString());
        partida.setData(response.get("data_partida").toString());

        Object preco = response.get("preco");
        if(preco != null)
            partida.setPreco(Double.parseDouble(preco.toString()));
        else
            partida.setPreco(0);

        String ingressos_disponiveis = response.get("ingressos_disponiveis").toString();
        if(!ingressos_disponiveis.isEmpty())
            partida.setIngressoDisponiveis( (int)Float.parseFloat(ingressos_disponiveis));


        Object time1 = response.get("time1");
        Object time2 = response.get("time2");
        if (time1 != null)
            partida.setTime1(time1.toString());

        if (time2 != null)
            partida.setTime2(time2.toString());



        BasicDBList ingressoResponse = (BasicDBList) response.get("ingressos");
        ArrayList<Ingresso> ingressos = new ArrayList<Ingresso>();
        if(ingressoResponse != null){

            for (int i = 0; i < ingressoResponse.size(); i++) {
                ingressos.add(this.createInstanceObjectIngressoBy((BasicDBObject) ingressoResponse.get(i)));
            }
        }
        partida.setIngressos(ingressos);
        return partida;
    }


    public ArrayList<Partida> getAllPartidas() {
        ArrayList<Partida> list = new ArrayList<Partida>();
        DBCursor cursor = collection.find();

        int i = 0;

        while (cursor.hasNext()) {
            list.add(i, createInstanceObjectBy(cursor.next()));
        }

        return list;
    }

    private Ingresso createInstanceObjectIngressoBy(BasicDBObject data) {
        Ingresso ingresso = new Ingresso();
        ingresso.setAcento((int)Float.parseFloat(data.get("acento").toString()));
        ingresso.setDataVenda(data.get("data_venda").toString());
        ingresso.setCpfCliente(data.get("cpfcliente").toString());
        ingresso.setStatus(data.get("status").toString());
        Object type = data.get("tipo");
        if(type != null){

            ingresso.setType(type.toString());
        }
        return ingresso;
    }

    private BasicDBObject getObjectDataDB(Partida partida) {
        BasicDBObject data = new BasicDBObject();

        //Estadio
        String estadio = partida.getEstadio();
        if (estadio != null) data.append("estadio", estadio);

        //Titulo
        String titulo = partida.getTitulo();
        if (titulo != null) data.append("titulo", titulo);

        //Data da Partida
        String data_partida = partida.getData();
        if (data_partida != null) data.append("data_partida", data_partida);

        //Selecao
        String time1 = partida.getTime1();
        if (time1 != null) data.append("time1", time1);

        String time2 = partida.getTime2();
        if (time2 != null) data.append("time2", time2);

        //Ingresso
        ArrayList<Ingresso> ingressos = partida.getIngressos();

        BasicDBList ingressosData = new BasicDBList();
        if (ingressos != null) {
            if(ingressos.size() > 0){

                for (int i = 0; i < ingressos.size(); i++) {
                    ingressosData.add(this.getObjectDataDBIngresso(ingressos.get(i)));
                }
                data.append("ingressos", ingressosData);
            }
        }

        //Preco
        double preco = partida.getPreco();
        if (preco != 0) data.append("preco", preco);

        //Ingressos Disponiveis(quantidade)
        int ingressoDisponiveis = partida.getIngressoDisponiveis();
        if (ingressoDisponiveis != 0) data.append("ingressos_disponiveis", ingressoDisponiveis);

        System.out.println(data);
        return data;
    }

    private DBObject getObjectDataDBIngresso(Ingresso ingresso) {
        BasicDBObject data = new BasicDBObject();

        data.append("acento", ingresso.getAcento());
        data.append("status", ingresso.getStatus());
        data.append("cpfcliente", ingresso.getCpfCliente());
        data.append("data_venda", ingresso.getDataVenda());
        data.append("tipo", ingresso.getType());

        return data;
    }

    public Boolean create(Partida partida) {
        try{
            collection.insert(getObjectDataDB(partida));
            return true;
        } catch (Exception e){
            return false;
        }
    }

    public boolean update(Partida partida) {
        try{

            BasicDBObject data = this.getObjectDataDB(partida);
            collection.update(partida.getDbObject(), data);
            System.out.println("Partida atualizada com sucesso");
            return true;
        } catch (Exception e){
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return false;
        }
    }

    public boolean deletePartida(Partida partida) {
        try{
            collection.remove(partida.getDbObject());
            return true;
        } catch (Exception e){
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return false;
        }
    }

    static String readFile(String path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    public  ArrayList<Partida> search(String partidaTitulo, String cpf, String dataVenda){

        /*
            db.partidas.aggregate(
               { $unwind: '$ingressos'},
               //{ $match: {'ingressos.cliente': '43170901800'}},
               { $sort: {'ingressos.data_venda': 1}}
            )

            { $text: { $search: <string>, $language: <string> } }
        * */

        String fileJSContent = null;
        ArrayList<Partida> partidas = new ArrayList<Partida>();
            try {
            fileJSContent = readFile("scripts/oki.js", Charset.defaultCharset());
            fileJSContent = String.format(fileJSContent, partidaTitulo, cpf, dataVenda);
        } catch (IOException e) {
            e.printStackTrace();
        }
        CommandResult result = db.doEval(fileJSContent);
        double resultCode = Double.parseDouble(result.get("ok").toString());
        if(resultCode == 1.0){
            BasicDBList a = (BasicDBList) result.get("retval");
            for(int i = 0; i < a.size(); i++){
                partidas.add(i, createInstanceObjectBy((DBObject) a.get(i)));
            }
        } else {

            System.err.println("Errrors");
            return null;
        }

        return partidas;
    }
}
