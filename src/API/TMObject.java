package API;

import com.mongodb.DBObject;

/**
 * @Project - MTP
 * User: Guilherme
 * Date: 4/10/2014 - 8:45 PM
 * API - <Description>
 */
public class TMObject {
    private DBObject dbObject;

    public DBObject getDbObject() {
        return dbObject;
    }

    public void setDbObject(DBObject dbObject) {
        this.dbObject = dbObject;
    }
}
