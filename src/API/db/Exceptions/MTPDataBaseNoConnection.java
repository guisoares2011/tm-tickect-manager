package API.db.Exceptions;

/**
 * @Project - MTP
 * User: Guilherme
 * Date: 4/5/14 - 5:35 PM
 * API.db.Exceptions - <Description>
 */
public class MTPDataBaseNoConnection extends Exception {
    public MTPDataBaseNoConnection(String message) {
        super(message);
    }
}
