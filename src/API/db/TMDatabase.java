package API.db;

import API.db.Exceptions.MTPDataBaseNoConnection;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

/**
 * @Project - MTP
 * User: Guilherme
 * Date: 4/5/14 - 4:09 PM
 * API.db - <Description>
 */

public class TMDatabase {

    DB db;
    MongoClient mongoDB = null;
    String DATA_BASE_NAME = "TMServer";

    public TMDatabase() {

        try {
            mongoDB = new MongoClient("localhost");
            db = mongoDB.getDB(DATA_BASE_NAME);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public DB getDB() {
        return this.db;
    }

    public MongoClient getMongoDB() throws MTPDataBaseNoConnection {
        if (mongoDB == null) {
            throw new MTPDataBaseNoConnection("No has connection with Mongo Database!");
        }
        return mongoDB;
    }

    public DBCollection getCollection(String name) {
        return db.getCollection(name);
    }

}
