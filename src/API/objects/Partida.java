package API.objects;

import API.TMApi;
import API.TMObject;
import com.mongodb.DBObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * @Project - TM
 * User: Guilherme
 * Date: 4/3/14 - 8:31 PM
 * API.objects - <Description>
 */
public class Partida extends TMObject {

    private ArrayList<Ingresso> ingressos = new ArrayList<Ingresso>();
    private String titulo, data, time1, time2, estadio;
    private int ingressoDisponiveis;
    private double preco;


    public boolean update(){
        return TMApi.updatePartida(this);
    }

    public ArrayList<Ingresso> getIngressos() {
        return ingressos;
    }

    public void setIngressos(ArrayList<Ingresso> ingressos) {
        this.ingressos = ingressos;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTime1() {
        return time1;
    }

    public void setTime1(String time1) {
        this.time1 = time1;
    }

    public String getTime2() {
        return time2;
    }

    public void setTime2(String time2) {
        this.time2 = time2;
    }

    public String getEstadio() {
        return estadio;
    }

    public void setEstadio(String estadio) {
        this.estadio = estadio;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public int getIngressoDisponiveis() {
        return ingressoDisponiveis;
    }

    public void setIngressoDisponiveis(int ingressoDisponiveis) {
        this.ingressoDisponiveis = ingressoDisponiveis;
    }

    public void purchase(String cpfcliente, String type) {
        Ingresso ingresso = new Ingresso();
        ingresso.setAcento(this.ingressos.size() + 1);
        ingresso.setStatus("Vendido");
        ingresso.setType(type);
        ingresso.setCpfCliente(cpfcliente);
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        ingresso.setDataVenda(dateFormat.format(date));
        this.ingressos.add(ingresso);
        this.update();
        System.out.println("Ingresso comprado com sucesso");
    }

    public void delete(){
        TMApi.deletePartida(this);
    }

    public Boolean create() {
        return TMApi.createPartida(this);
    }

    @Override
    public String toString(){
        return getTitulo();
    }
}
