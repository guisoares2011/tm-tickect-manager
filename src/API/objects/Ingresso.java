package API.objects;

import API.TMObject;

/**
 * @Project - MTP
 * User: Guilherme
 * Date: 4/3/14 - 8:30 PM
 * API.objects - <Description>
 */
public class Ingresso extends TMObject {

    private int acento;
    private String status, type = "Comum", cpfCliente, dataVenda;

    public String getCpfCliente() {
        return cpfCliente;
    }

    public void setCpfCliente(String cpfCliente) {
        this.cpfCliente = cpfCliente;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    public int getAcento() {

        return acento;
    }

    public void setAcento(int acento) {

        this.acento = acento;
    }

    public String getDataVenda() {
        return dataVenda;
    }

    public void setDataVenda(String dataVenda) {
        this.dataVenda = dataVenda;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }

    public String toString(){
        return "Acento: " + this.getAcento() +
               "\nData Venda: " + this.getDataVenda() +
               "\nTipo: " + this.getType();
    }
}
