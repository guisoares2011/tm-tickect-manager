package API;

/**
 * @Project - MTP
 * @author Guilherme
 * Date: 4/3/14 - 8:26 PM
 * API -
 */

import API.db.TMDatabase;
import API.management.EstadioManagement;
import API.management.PartidaManagement;
import API.management.SelecaoManagement;
import API.objects.Partida;

import java.util.ArrayList;

public class TMApi {

    private static TMDatabase db;
    private static ArrayList allPartidas;

    public static void init() {
        db = new TMDatabase();
    }

    /**
     * Retorna o Objeto que faz conexão com o Mongo DB
     *
     * @return TMDatabase Object
     */
    private static TMDatabase getDB() {
        return TMApi.db;
    }

    /**
     * Retorna uma lista com todas as seleções.
     *
     * @return ArrayList<Object>
     */
    public static ArrayList<Object> getAllSelecao() {

        return new SelecaoManagement(db).getAll();
    }

    /**
     * Retorna a lista completa de estadios.
     *
     * @return ArrayList<Object>
     */
    public static ArrayList<Object> getAllEstadio() {
        return new EstadioManagement(db).getAll();
    }

    /**
     * Cria uma partida no banco de dados a partir do objeto Partida passado no
     * parametro p.
     *
     * @param partida Objeto Partida com todas informações da partida
     * @return Retorna Boolean(true ou false) com o status da operacao. True: Sucesso, False: Error ou já registrado
     */
    public static Boolean createPartida(Partida partida) {
        return new PartidaManagement(db).create(partida);
    }

    /**
     * Retorna lista de partidas de acordo com as informações do parametro p
     *
     * @param p Objeto Partida possuindo as propriedade que você deseja pesquisa
     *          Exemplo:
     *          <p/>
     *          Se deseja pesquisa todas as partida que tenha o time1 como argentina<br>
     *          <i>
     *          Partida p = new Partida();<br>
     *          p.setTime1("Argentina");<br>
     *          ArrayList<Object> result = TMApi.getPartida(p);<br>
     *          </i>
     */
    public static ArrayList<Partida> getPartida(Partida p) {

        return new PartidaManagement(db).get(p);
    }

    /**
     * Retorna lista de partidas de acordo com o titulo
     *
     * @param titulo Titulo da partida
     * @see #getPartida
     */
    public static ArrayList<Partida> getPartidasByTitulo(String titulo) {
        Partida p = new Partida();
        p.setTitulo(titulo);
        return getPartida(p);
    }


    public static boolean updatePartida(Partida partida) {
        return new PartidaManagement(db).update(partida);
    }

    public static ArrayList<Partida> getAllPartidas() {
        return new PartidaManagement(db).getAllPartidas();
    }

    public static boolean deletePartida(Partida partida) {
        return new PartidaManagement(db).deletePartida(partida);
    }

    public static ArrayList<Partida> searchPartida(String partidaTitulo, String cpf, String nome){
        return new PartidaManagement(db).search(partidaTitulo, cpf, nome);
    }
}
