package autoloader;

import java.io.*;
import java.nio.file.Path;

/**
 * @Project - TIO
 * User: Guilherme
 * Date: 4/20/2014 - 1:25 PM
 * autoloader - <Description>
 */
public class HandlerProcess {

    static BufferedReader input;

    public static String getPathToMongoDB() throws FileNotFoundException{
        String program = "C:\\mongodb\\bin\\mongod.exe";
        String databaseFolder = System.getProperty("user.dir") + "\\data";
        File databaseStorage = new File(databaseFolder);
        if (!(databaseStorage.exists() && databaseStorage.isDirectory())) {
            throw new FileNotFoundException("A pasta do banco da dados não pode ser encontrada.");
        }
        File mongodLock = new File(databaseFolder + "\\mongod.lock");
        if(mongodLock.exists()){
            mongodLock.delete();
        }

        File databaseProcess = new File(program);
        if (!databaseProcess.exists()) {
            throw new FileNotFoundException("O executavel para abir o processo do mongo db não foi encontrado");
        }
        String cmd = program + " --dbpath \"" + databaseFolder + "\" --setParameter textSearchEnabled=true";
        System.out.println(cmd);
        return cmd;
    }
}
