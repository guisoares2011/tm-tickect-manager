package autoloader;

import API.TMApi;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * @Project - TIO
 * User: Guilherme
 * Date: 4/20/2014 - 3:19 PM
 * autoloader - <Description>
 */
public class MongoDBStart implements Runnable {

    public void run() {

        try {
            String line;
            Process p = Runtime.getRuntime().exec(HandlerProcess.getPathToMongoDB());
            HandlerProcess.input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            TMProcess tmProcess = new TMProcess();
            TMApi.init();
            Thread TMinit = new Thread(tmProcess);
            TMinit.start();
            while ((line = HandlerProcess.input.readLine()) != null) {

                System.out.println(line);
            }
            HandlerProcess.input.close();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }
}
