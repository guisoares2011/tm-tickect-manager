package autoloader;

import java.io.IOException;

/**
 * @Project - TIO
 * User: Guilherme
 * Date: 4/20/2014 - 12:58 PM
 * autoloader - <Description>
 */

public class ShutdownMongoBD extends Thread{
    public void run() {
        try {
            HandlerProcess.input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}