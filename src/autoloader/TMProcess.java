package autoloader;

import ui.MainFrame;

/**
 * @Project - TIO
 * User: Guilherme
 * Date: 4/20/2014 - 5:00 PM
 * autoloader - <Description>
 */
public class TMProcess implements Runnable{

    /**
     * Metodo para iniciar o desenvolvimento do programa.
     * */
    public void run() {
        /*
        * Começe o codigo de Voces AQUI
        *
        *
        * */
        MainFrame main = new MainFrame();
        main.setVisible(true);
    }
}
