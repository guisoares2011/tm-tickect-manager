/**
 * @Project - MTP
 * User: Guilherme
 * Date: 4/3/14 - 9:44 PM
 * PACKAGE_NAME - <Description>
 */
//import API.TMApi;
//import API.objects.Selecao;
import autoloader.MongoDBStart;
import autoloader.ShutdownMongoBD;

public class Main {
    /**
     *
     * Inicia 2 threads,
     * 1 - Inicia o processo do mongo DB
     * 2 - Inicializa o processo para criar as telas(logica que seria da main)
     *
     * O arquivo onde vai conter a logica para abrir as telas necessario executar
     * pois ela espera o processo do mongo db ser aberto
     *
     * @see autoloader.TMProcess
     * */
    public static void main(String[] args) {
        MongoDBStart mongo = new MongoDBStart();
        Thread loaderMongo = new Thread(mongo);
        loaderMongo.start();

        ShutdownMongoBD shutdownInterceptor = new ShutdownMongoBD();
        Runtime.getRuntime().addShutdownHook(shutdownInterceptor);

    }
}
