package ui.pages;

import javax.swing.*;

/**
 * @Project - vadia
 * User: Guilherme
 * Date: 5/25/2014 - 1:21 PM
 * ui.pages - <Description>
 */
public interface PageInterface {

    public void build(Object...n);
}
