package ui.pages;

import API.TMApi;
import API.objects.Partida;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * @Project - vadia
 * User: Guilherme
 * Date: 6/8/2014 - 3:42 PM
 * ui.pages - <Description>
 */
public class VisualizarPartidas extends TMPanel implements ActionListener, PageInterface {

    private JLabel
            visualizarPartidas,
            dataLabel,
            partidaLabel,
            tituloPagina;

    private JTextField partida;
    private JTextField data;
    private JButton pesquisar, editar;
    private JTable table;
    private Object itemSelected;

    private ArrayList<Partida> listPartidas = new ArrayList<Partida>();
    private DefaultTableModel model;

    public VisualizarPartidas(){

        tituloPagina = new JLabel("Visualizar Partidas");
        tituloPagina.setBounds(168, 68, 200, 30);
        this.add(tituloPagina);

        data = new JTextField();
        data.setBounds(395, 140, 133, 29);
        data.setVisible(true);
        this.add(data);

        String [] cols = new String[]{"Nome do Jogo", "Time 1º", "Time 2º", "Data"};
        String [][] rows = new String[][]{};

        model = new DefaultTableModel(rows, cols);
        table = new JTable(model){
            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setBounds(165, 200, 457, 200);
        final VisualizarPartidas self = this;
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent event) {
                self.showEditButton();
            }
        });
        JScrollPane scroll = new JScrollPane(table);
        scroll.setBounds(165, 200, 457, 200);
        this.add(scroll);

        visualizarPartidas = new JLabel("Visualizar Partidas");

        dataLabel = new JLabel("Data");
        dataLabel.setBounds(402, 116, 200, 30);
        this.add(dataLabel);

        partidaLabel = new JLabel("Partida");
        partidaLabel.setBounds(171, 116, 200, 30);
        this.add(partidaLabel);

        partida = new JTextField();
        partida.setBounds(165,140, 217, 29);
        this.add(partida);

        pesquisar = new JButton("Pesquisar");
        pesquisar.setBounds(538,139,90, 31);
        pesquisar.addActionListener(this);
        this.add(pesquisar);

        editar = new JButton("Editar");
        editar.setBounds(540, 410, 80, 31);
        editar.setVisible(false);
        editar.addActionListener(this);
        this.add(editar);
    }

    public void showEditButton() {
        editar.setVisible(true);
    }

    public void adicionaLinha(String...cols){
        DefaultTableModel modelo = (DefaultTableModel) getTabela().getModel();
        modelo.addRow(cols);
    }

    public void build(Object...n){
        this.limpaTabela();
        partida.setText("");
        data.setText("");
    }

    public JTable getTabela() {
        return table;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == editar) {
            getjFrame().changePage(getjFrame().editarPartida, getItemSelected());
        } else if (e.getSource() == pesquisar) {
            search();
        } else {
            super.actionPerformed(e);
        }
    }

    private void limpaTabela(){
        if (model.getRowCount() > 0) {
            for (int i = model.getRowCount() - 1; i > -1; i--) {
                model.removeRow(i);
            }
        }
    }

    private void search() {
        Partida p = new Partida();
        String partidaTitulo = partida.getText();
        String partidaData = data.getText();
        if(!partidaData.isEmpty()) p.setData(partidaData);
        if(!partidaTitulo.isEmpty()) p.setTitulo(partidaTitulo);
        listPartidas = TMApi.getPartida(p);
        limpaTabela();
        System.out.println(p);
        System.out.println(listPartidas);
        for(Partida partida : listPartidas)
            adicionaLinha(partida.getTitulo(), partida.getTime1(), partida.getTime2(), partida.getData());
        hideEditButton();
    }

    private void hideEditButton() {
        editar.setVisible(false);
    }



    public Object getItemSelected() {
        int index = table.getSelectedRow();
        return listPartidas.get(index);
    }
}
