package ui.pages;

import API.TMApi;
import API.objects.Ingresso;
import API.objects.Partida;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

public class VisualizaoCompras extends TMPanel implements PageInterface, ActionListener {

    private JLabel partidaLabel, cpfLabel, dataLabel, tituloPagina;
    private JTextField partidaInput, cpfInput, dataInput;
    private JButton botaoBuscar;
    private JTable table;
    private DefaultTableModel model;

    public VisualizaoCompras() {

        tituloPagina = new JLabel("Visualização das Vendas");
        tituloPagina.setBounds(168, 68, 200, 30);
        this.add(tituloPagina);

        partidaLabel = new JLabel("Partida");
        partidaLabel.setBounds(171, 116, 200, 30);
        this.add(partidaLabel);

        cpfLabel = new JLabel("CPF Cliente");
        cpfLabel.setBounds(171, 177, 200, 30);
        this.add(cpfLabel);

        dataLabel = new JLabel("Data");
        dataLabel.setBounds(402, 116, 200, 30);
        this.add(dataLabel);

        partidaInput = new JTextField();
        partidaInput.setBounds(165,140, 217, 29);
        this.add(partidaInput);

        cpfInput = new JTextField("");
        cpfInput.setBounds(166, 200, 217, 29);
        this.add(cpfInput);

        dataInput = new JTextField("");
        dataInput.setBounds(395, 140, 133, 29);
        this.add(dataInput);

        botaoBuscar = new JButton("Pesquisar");
        botaoBuscar.setBounds(538, 140, 90, 31);
        botaoBuscar.addActionListener(this);
        this.add(botaoBuscar);

        String [] cols = new String[]{"Nome do Jogo", "CPF Cliente", "Valor", "Data"};
        String [][] rows = new String[][]{};
        model = new DefaultTableModel(rows, cols);
        table = new JTable(model){
            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scroll = new JScrollPane(table);
        scroll.setBounds(165, 240, 457, 200);
        this.add(scroll);
    }

    public void adicionaLinha(String...cols){
        DefaultTableModel modelo = (DefaultTableModel) getTable().getModel();
        modelo.addRow(cols);
	}

    public JTable getTable() {
        return table;
    }

    private void limpaTabela(){
        if (model.getRowCount() > 0) {
            for (int i = model.getRowCount() - 1; i > -1; i--) {
                model.removeRow(i);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (botaoBuscar == e.getSource()) {

            limpaTabela();

            String titulo= partidaInput.getText();
            String data= dataInput.getText();
            String cpf= cpfInput.getText();
            ArrayList<Partida> partidas = TMApi.searchPartida(titulo, data, cpf);
            for (Partida partida : partidas){
                ArrayList<Ingresso> ingressos = partida.getIngressos();
                for (Ingresso ingresso : ingressos) {
                    adicionaLinha(partida.getTitulo(), ingresso.getCpfCliente(), String.valueOf(partida.getPreco()), ingresso.getDataVenda());
                }
            }
        } else {
            super.actionPerformed(e);
        }
    }
}