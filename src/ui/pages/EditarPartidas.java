package ui.pages;

import API.TMApi;
import API.objects.Partida;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * @Project - vadia
 * User: Guilherme
 * Date: 6/8/2014 - 7:16 PM
 * ui.pages - <Description>
 */
public class EditarPartidas extends TMPanel implements ActionListener, PageInterface {
    JLabel tituloPagina, tituloLabel, numIngressoLabel, estadioLabel, dataLabel, timesLabel, precosLabel;
    JTextField tituloDaPartidaField, numeroDeIngressosField, dataField, precoField;
    JComboBox<Object> estadioList;
    JComboBox<Object> time1List;
    JComboBox<Object> time2List;
    JButton concluirButton;
    private Partida partidaEditar;

    private ArrayList<Object> allSelecao = TMApi.getAllSelecao();
    private ArrayList<Object> allEstadio = TMApi.getAllEstadio();

    public EditarPartidas() {
        //Logic...
        tituloPagina = new JLabel("Cadastrar Partidas");
        tituloLabel = new JLabel("Titulo da Partida");
        numIngressoLabel = new JLabel("Nº de Ingressos");
        estadioLabel = new JLabel("Estadio");
        dataLabel = new JLabel("Data");
        timesLabel = new JLabel("Times");
        precosLabel = new JLabel("Preços");

        tituloDaPartidaField = new JTextField();
        numeroDeIngressosField = new JTextField();
        dataField = new JTextField();
        precoField = new JTextField();
        estadioList = new JComboBox<Object>(allEstadio.toArray());
        time1List = new JComboBox<Object>(allSelecao.toArray());
        time2List = new JComboBox<Object>(allSelecao.toArray());

        concluirButton = new JButton("Concluir");
        tituloPagina.setBounds(168, 68, 200, 30);//titulo //(posição x, posição y, Largura, Altura)
        tituloLabel.setBounds(171, 116, 200, 30);//titulo partida
        tituloDaPartidaField.setBounds(165, 140, 219, 29);
        numIngressoLabel.setBounds(402, 177, 200, 30);//ingressos
        numeroDeIngressosField.setBounds(395, 200, 133, 29);
        estadioLabel.setBounds(171, 177, 200, 30);//estadios
        estadioList.setBounds(166, 200, 217, 29);
        dataLabel.setBounds(402, 116, 200, 30);//Data
        dataField.setBounds(395, 139, 133, 29);
        timesLabel.setBounds(171, 237, 217, 30);// times
        time1List.setBounds(166, 260, 217, 29);
        time2List.setBounds(395, 260, 217, 29);
        precosLabel.setBounds(171, 290, 133, 30);//preço
        precoField.setBounds(166, 315, 133, 30);
        concluirButton.setBounds(513, 350, 100, 30);

        this.add(tituloPagina);
        this.add(tituloLabel);
        this.add(numIngressoLabel);
        this.add(estadioLabel);
        this.add(dataLabel);
        this.add(timesLabel);
        this.add(precosLabel);
        this.add(tituloDaPartidaField);
        this.add(numeroDeIngressosField);
        this.add(dataField);
        this.add(precoField);
        this.add(estadioList);
        this.add(time1List);
        this.add(time2List);
        this.add(concluirButton);

        concluirButton.addActionListener(this);

    }
    public void clearFields() {
        tituloDaPartidaField.setText("");
        numeroDeIngressosField.setText("");
        dataField.setText("");
        precoField.setText("");
    }

    public void build(Object...n){
        int index;
        if(n.length > 0){
            partidaEditar = (Partida) n[0];

            tituloDaPartidaField.setText(partidaEditar.getTitulo());
            numeroDeIngressosField.setText(Integer.toString(partidaEditar.getIngressoDisponiveis()));
            precoField.setText(Double.toString(partidaEditar.getPreco()));
            estadioList.setToolTipText(partidaEditar.getEstadio());
            dataField.setText(partidaEditar.getData());
            String time1 = partidaEditar.getTime1();
            String time2 = partidaEditar.getTime2();
            String estadio =  partidaEditar.getEstadio();

            if(time1 != null){
                index = allSelecao.indexOf(time1);
                System.out.println(index);
                time1List.setSelectedItem(time1List.getItemAt(index));
            }

            if(time2 != null){
                index = allSelecao.indexOf(time2);
                time2List.setSelectedItem(time2List.getItemAt(index));
            }

            if(estadio != null){
                index = allEstadio.indexOf(estadio);
                estadioList.setSelectedItem(estadioList.getItemAt(index));
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {


        if (actionEvent.getSource() == concluirButton)//Cadastrar
        {
            /*
            Ajustei o metodo para outra funcao para ficar mais limpo
            * */
            if (this.updatePartida()) {
                JOptionPane.showMessageDialog(null, "Atualizado");
            } else {
                JOptionPane.showMessageDialog(null, "Erro ao atualizar");
            }

        } else {
            super.actionPerformed(actionEvent);
        }
    }
    private boolean updatePartida(){
        partidaEditar.setTitulo(tituloDaPartidaField.getText());

        String IngressoDisponiveis = numeroDeIngressosField.getText();
        String preco = precoField.getText();

        if(IngressoDisponiveis.isEmpty()) {

            partidaEditar.setIngressoDisponiveis(0);
        } else {
            partidaEditar.setIngressoDisponiveis(Integer.parseInt(numeroDeIngressosField.getText()));
        }

        if(preco.isEmpty()){

            partidaEditar.setPreco(0);
        } else {

            partidaEditar.setPreco(Double.parseDouble(precoField.getText()));
        }

        /* Selecinado Partidas*/

        String time1 = partidaEditar.getTime1();

        partidaEditar.setData(dataField.getText());
        partidaEditar.setEstadio(estadioList.getSelectedItem().toString());
        partidaEditar.setTime1(time1List.getSelectedItem().toString());
        partidaEditar.setTime2(time2List.getSelectedItem().toString());

        return partidaEditar.update();
    }
}
