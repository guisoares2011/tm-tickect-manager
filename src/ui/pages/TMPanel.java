package ui.pages;

import ui.MainFrame;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class TMPanel extends JPanel implements PageInterface, ActionListener {

    JLabel menuLabel, infoLabel;
    JButton novaCompra, novaPartida, visualizarCompras, visualizarPartidas;

    public MainFrame getjFrame() {
        return jFrame;
    }

    public void setjFrame(MainFrame jFrame) {
        this.jFrame = jFrame;
    }

    private MainFrame jFrame;

    public TMPanel() {

        menuLabel = new JLabel("Menu");
        menuLabel.setBounds(10, 10, 150, 30);

        infoLabel = new JLabel("Escolha uma das opções da esquerda!");
        infoLabel.setBounds(250, 200, 150, 50);

        novaCompra = new JButton("Nova Compra");
        novaCompra.setBounds(10, 60, 150, 50);
        novaCompra.addActionListener(this);

        novaPartida = new JButton("Nova Partida");
        novaPartida.setBounds(10, 120, 150, 50);
        novaPartida.addActionListener(this);

        visualizarCompras = new JButton("Vizualizar Compras");
        visualizarCompras.setBounds(10, 180, 150, 50);
        visualizarCompras.addActionListener(this);

        visualizarPartidas = new JButton("Vizualizar Partida");
        visualizarPartidas.setBounds(10, 240, 150, 50);
        visualizarPartidas.addActionListener(this);

        add(menuLabel);
        add(novaCompra);
        add(novaPartida);
        add(visualizarCompras);
        add(visualizarPartidas);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == novaCompra) {

        } else if (e.getSource() == novaPartida){

            jFrame.changePage(jFrame.cadastrarPartida);

        } else if (e.getSource() == visualizarCompras) {

            jFrame.changePage(jFrame.visualizaoCompras);

        } if (e.getSource() == visualizarPartidas) {
             jFrame.changePage(jFrame.visualizarPartidas);
        }
    }

    @Override
    public void build(Object... n) {

    }
}
