package ui.pages;

import API.TMApi;
import API.objects.Partida;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JComboBox;


/**
 * Created by Novo_Samsung on 22/05/2014.
 */
public class CadastrarPartida extends TMPanel implements PageInterface, ActionListener {

    JLabel  tituloPagina, tituloLabel, numIngressoLabel, estadioLabel, dataLabel, timesLabel, precosLabel;
    JTextField tituloDaPartidaField, numeroDeIngressosField, dataField, precoField;
    JComboBox<Object> estadioList;
    JComboBox<Object> time1List;
    JComboBox<Object> time2List;
    JButton concluirButton;

    public CadastrarPartida() {
        //Logic...
        tituloPagina = new JLabel("Cadastrar Partidas");
        tituloLabel = new JLabel("Titulo da Partida");
        numIngressoLabel = new JLabel("Nº de Ingressos");
        estadioLabel = new JLabel("Estadio");
        dataLabel = new JLabel("Data");
        timesLabel = new JLabel("Times");
        precosLabel = new JLabel("Preços");

        tituloDaPartidaField = new JTextField();
        numeroDeIngressosField = new JTextField();
        dataField = new JTextField();
        precoField = new JTextField();


        ArrayList<Object> allSelecao = TMApi.getAllSelecao();
        estadioList = new JComboBox<Object>(TMApi.getAllEstadio().toArray());
        time1List = new JComboBox<Object>(allSelecao.toArray());
        time2List = new JComboBox<Object>(allSelecao.toArray());

        concluirButton = new JButton("Concluir");


        tituloPagina.setBounds(168, 68, 200, 30);//titulo //(posição x, posição y, Largura, Altura)
        tituloLabel.setBounds(171, 116, 200, 30);//titulo partida
        tituloDaPartidaField.setBounds(165, 140, 219, 29);
        numIngressoLabel.setBounds(402, 177, 200, 30);//ingressos
        numeroDeIngressosField.setBounds(395, 200, 133, 29);
        estadioLabel.setBounds(171, 177, 200, 30);//estadios
        estadioList.setBounds(166, 200, 217, 29);
        dataLabel.setBounds(402, 116, 200, 30);//Data
        dataField.setBounds(395, 139, 133, 29);
        timesLabel.setBounds(171, 237, 217, 30);// times
        time1List.setBounds(166, 260, 217, 29);
        time2List.setBounds(395, 260, 217, 29);
        precosLabel.setBounds(171, 290, 133, 30);//preço
        precoField.setBounds(166, 315, 133, 30);
        concluirButton.setBounds(513, 350, 100, 30);

        this.add(tituloPagina);
        this.add(tituloLabel);
        this.add(numIngressoLabel);
        this.add(estadioLabel);
        this.add(dataLabel);
        this.add(timesLabel);
        this.add(precosLabel);
        this.add(tituloDaPartidaField);
        this.add(numeroDeIngressosField);
        this.add(dataField);
        this.add(precoField);
        this.add(estadioList);
        this.add(time1List);
        this.add(time2List);
        this.add(concluirButton);

        concluirButton.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {

        if(actionEvent.getSource()== concluirButton)//Cadastrar
        {
            /*
            Ajustei o metodo para outra funcao para ficar mais limpo
            * */
             if(this.cadastrarPartida()){
                 JOptionPane.showMessageDialog(null,"Cadastrado");
             } else {
                 JOptionPane.showMessageDialog(null,"Erro ao cadastrar");
             }

        } else {
            super.actionPerformed(actionEvent);
        }
    }

    private boolean cadastrarPartida(){
        Partida p = new Partida();
        p.setTitulo(tituloDaPartidaField.getText());

        String IngressoDisponiveis = numeroDeIngressosField.getText();
        String preco = precoField.getText();

        if(IngressoDisponiveis.isEmpty()) {

            p.setIngressoDisponiveis(0);
        } else {
            p.setIngressoDisponiveis(Integer.parseInt(numeroDeIngressosField.getText()));
        }

        if(preco.isEmpty()){

            p.setPreco(0);
        } else {

            p.setPreco(Double.parseDouble(precoField.getText()));
        }

        p.setData(dataField.getText());
        p.setEstadio(estadioList.getSelectedItem().toString());
        p.setTime1(time1List.getSelectedItem().toString());
        p.setTime2(time2List.getSelectedItem().toString());

        return TMApi.createPartida(p);
    }
}
