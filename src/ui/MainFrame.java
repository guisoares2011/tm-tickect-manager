package ui;

import ui.pages.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;

/**
 * @Project - vadia
 * User: Guilherme
 * Date: 5/25/2014 - 12:44 PM
 * ui - <Description>
 */
public class MainFrame extends JFrame implements ActionListener{

    private String titulo;
    public  TMPanel paginaPrincipal = new TMPanel();
    public TMPanel cadastrarPartida = new CadastrarPartida();
    public TMPanel visualizaoCompras = new VisualizaoCompras();
    public TMPanel visualizarPartidas = new VisualizarPartidas();
    public TMPanel editarPartida = new EditarPartidas();


    public MainFrame()  {
        setTitle("TM - Ticket Manager");
        setSize(640, 480);
        setLocation(400, 110);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        changePage(paginaPrincipal);
    }

    public void changePage(TMPanel page, Object...values) {
        page.setjFrame(this);
        page.setLayout(null);
        page.setSize(this.getSize());
        getContentPane().removeAll();
        getContentPane().add(page, BorderLayout.CENTER);
        getContentPane().doLayout();
        update(getGraphics());
        page.build(values);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

}
