
function filter(results){
    var filter_result = {};
    for(var i=0; i < results.length; i++){
        var result = results[i];
        var map_result = {};
        var count_map = 0, id;
        if(result.ingressos.acento == 1){
            if(result._id in map_result){
                id = map_result[result._id];
                filter_result[id].ingressos.push(result.ingressos);
            } else {
                map_result[result._id] = count_map;
                filter_result[count_map] = result;
                filter_result[count_map].ingressos = [result.ingressos]
                count_map++;
            }
        }
    }
    return filter_result.sort();
}

filter(db.partidas.aggregate(
  { $unwind: '$ingressos'},
  { $sort: {'ingressos.acento': 1}}
).result);