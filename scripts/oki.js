var IS_WEBSTORM_APPLICATION = false;
return (function(){

    /*if(!ObjectId){
        var ObjectId = function(k){return k};
    }

    if(!console){
        var console = {};
        console.log = function(){

        }
    }
*/
    /*if(!db){
        IS_WEBSTORM_APPLICATION = true;
        var db = {
            partidas: {
                aggregate: function(){
                    return {
                        "result" : [
                            {
                                "_id" : ObjectId("5397608c40c6a5d090005854"),
                                "titulo" : "4 de Final",
                                "data_partida" : "14/11/2012",
                                "time1" : "URUGUAI",
                                "time2" : "EQUADOR",
                                "ingressos" : {
                                    "acento" : 2,
                                    "status" : "Vendido",
                                    "cpfcliente" : "4333333333333",
                                    "data_venda" : "11/06/2014",
                                    "tipo" : "Comum"
                                },
                                "preco" : 200,
                                "ingressos_disponiveis" : 100
                            }
                        ],
                        "ok" : 1
                    }
                }
            }
        }
    }
     */
    var filter = function(results, titulo, cpf, data){
        var __results = [], result;
        var __count__ = 0;
        var __map = [];
        var index, index_map;

        for(index in results){
            result = results[index];

            if (titulo.length > 0)
                if (result.titulo.indexOf(titulo) === -1)
                    continue;

            if(data.length > 0)
                if(result.data_venda.indexOf(data) === -1)
                    continue;

            if(cpf.length > 0)
                if(result.cpfcliente.indexOf(cpf) === -1)
                    continue;

            if((index_map = __map.indexOf(result._id)) === -1){

                index_map = __count__;
                __map.push(result._id);
                __count__++;
                __results[index_map] = result;
                __results[index_map].ingressos = [result.ingressos]
            } else {
                __results[index_map].ingressos.push(result.ingressos);
            }
        }

        return __results;
    };

    var response = filter(db.partidas.aggregate(
        { $unwind: '$ingressos'},
        { $sort: {'ingressos.data_venda': 1}}
    ).result, '%s', '%s', '%s');
    return response;
})();


